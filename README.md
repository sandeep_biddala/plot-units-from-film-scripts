# README #


### What is this repository for? ###

* This code automatically generates [plot units](http://onlinelibrary.wiley.com/doi/10.1207/s15516709cog0504_1/abstract) for scenes from a movie script by parsing it and using some simple heuristics.
* The project produces images of the plot units scene wise for a movie script.
* Some of the heuristics and the general idea for the work is taken from [here.](http://www.umiacs.umd.edu/~amit/Papers/CIjournal10.pdf)

### Requirements ###
The project requires the following python libraries
* pyparsing
* nltk
* Python wrapper for Stanford CoreNLP available from [here.](https://bitbucket.org/torotoki/corenlp-python)
*titlecase
* pygraph
* graphviz

### Usage ###
This python script can be used to generate plot units for scripts which follow the following format or grammar

```
#!python

#film -> scene+
#scene -> (scene_start)(scene_contents)+(transition_end)?
#scene_start -> slug | transition_start
#slug - (location_type)(location?)(" - "time?)?\n\n
#location_type -> "INT. "|"EXT. "|"EXT. / INT. "
#location -> text
#time -> text
#transition -> (transition_type)(text?):\n\n
#transition_type -> "FADE"|"DISSOLVE"|"CUT"|"WIPE"
#transition_start -> transition
#transition_end -> [ ]+transition
#text -> .*
#description -> text?\n\n
#scene_contents -> dial_start|desciption

```
For using the script:

```
#!bash

python core.py <film_script_as_text_file> <folder_for_storing_scenes> <folder_for_storing_graph_images>
```

### Preview ###
Examples for the images generated
![59.png](https://bitbucket.org/repo/MRL4Bn/images/287385712-59.png)
![67.png](https://bitbucket.org/repo/MRL4Bn/images/3627971060-67.png)


### Contributors ###
* Sandeep Reddy Biddala

### Who do I talk to? ###

* sandeep_biddala
* Other community or team contact