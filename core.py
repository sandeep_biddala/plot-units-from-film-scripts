import pyparsing as pp
import re
import sys
from sets import Set
from nltk import word_tokenize
from nltk.wsd import lesk
from corenlp import *
import operator
from nltk.stem.porter import *
from titlecase import titlecase
import json
from nltk.corpus import wordnet as wn
from pygraph.classes.digraph import digraph
from pygraph.readwrite.dot import write
import gv

##################################
#Grammar for Film Script Parsing#
#################################
#film -> scene+
#scene -> (scene_start)(scene_contents)+(transition_end)?
#scene_start -> slug | transition_start
#slug - (location_type)(location?)(" - "time?)?\n\n
#location_type -> "INT. "|"EXT. "|"EXT. / INT. "
#location -> text
#time -> text
#transition -> (transition_type)(text?):\n\n
#transition_type -> "FADE"|"DISSOLVE"|"CUT"|"WIPE"
#transition_start -> transition
#transition_end -> [ ]+transition
#text -> .*
#description -> text?\n\n
#scene_contents -> dial_start|desciption

##########################
#Regular Expression Flags#
##########################
reflags = re.MULTILINE | re.DOTALL

text = ".*?"
location_type_string ="^((INT\.? )|(EXT\.? )|(EXT\.? / INT\.? )|(INT\.? / EXT\.? ))"
slug_string = location_type_string + text + "\n\n\n*"
slug = pp.Regex(re.compile(slug_string,reflags)).leaveWhitespace()
paren_string = "^[ ]+\(" + text + "\)\n"
paren = pp.Regex(re.compile(paren_string,reflags)).leaveWhitespace()
char_name_string = "^[ ]+[A-Z][A-Z ]*(\(V\.O\.\))*[ ]*(\(CONT\'D\))*\n"
char_name = pp.Regex(re.compile(char_name_string,reflags)).leaveWhitespace()
dialog_content_string = "^[ ]+" + text + "\n\n\n*"
dialog_content = pp.Regex(re.compile(dialog_content_string,reflags)).leaveWhitespace()
dialog = pp.Group(char_name.setResultsName("char_name") + pp.Optional(paren).setResultsName("paren") + dialog_content.setResultsName("dialog_content"))
transition_type_string = "((FADE)|(CUT )|(DISSOLVE)|(WIPE)|(BLACK))"
transition = pp.Regex(re.compile(transition_type_string + text + ":\n\n\n*",reflags)).leaveWhitespace()
transition_start = pp.Group(pp.Regex(re.compile("^",reflags)).leaveWhitespace() + transition)
transition_end = pp.Group(pp.Regex(re.compile("^[ \-]+",reflags)).leaveWhitespace() + transition)
scene_start = pp.Group(pp.Optional(transition).setResultsName("transition") + slug.setResultsName("slug"))
description = pp.Group(pp.NotAny(slug | dialog | transition | transition_end | transition_start).leaveWhitespace() + pp.Regex(re.compile(text + "\n\n\n*",reflags)).leaveWhitespace())
#description = pp.NotAny(slug | dialog | transition_end | transition_start).leaveWhitespace() + pp.Regex(re.compile(text + "\n\n",reflags)).leaveWhitespace()
scene_contents = pp.Group(pp.OneOrMore(dialog.setResultsName("dialog", True).leaveWhitespace() | description.setResultsName("description", True).leaveWhitespace() | transition_start.setResultsName("transition_start") | transition_end.setResultsName("transition_end")))
scene = pp.Group(scene_start.setResultsName("scene_start") + scene_contents.setResultsName("scene_contents") + pp.Optional(transition_end).setResultsName("transition"))
film = pp.Group(pp.OneOrMore(scene).setResultsName("scene")).leaveWhitespace().setResultsName("film")

######################
#Final Parsed Results#
######################
film_file=sys.argv[1]
scenes_folder=sys.argv[2]
graphs_folder=sys.argv[3]
res = film.parseFile(sys.argv[1])
#res = film.parseFile("../shot_segmentation/lotr_script.txt");
#res = film.parseFile("yoyo")

#######################################
#Processing for characters from dialog#
#######################################


def process(text):
    text = text.split()
    for i in range(0,len(text)):
	if (text[i].split("'"))[0].isupper():
	    s=text[i].split("'")
	    text[i]=titlecase(s[0])
	    if (len(s)>1):
	        text[i]=text[i]+"'"+s[1]
    content = " ".join(text)
    return content

def process_dialogue(scene_content):
    char_name = scene_content["char_name"].strip()
    if "(CONT'D)"in char_name:
        char_name = char_name[0:-8].strip()
    if "(V.O.)" in char_name:
	char_name = char_name[0:-6].strip()
    char_name = process(char_name)
    content = char_name + ' says, '
    content = content + '"' + process(scene_content["dialog_content"]) + '"'
    return content
    

characters_in_scenes = {}
scene_locations = []
plot_units_in_scenes = {}
adjacency_lists_in_scenes = {}
#full_content = ""

for i in range(0, len(res["film"]["scene"])):
    '''
    characters = Set()
    if ("dialog" in res["film"]["scene"][i]["scene_contents"].keys()):
        for j in range(0,len(res["film"]["scene"][i]["scene_contents"]["dialog"])):
            characters.add(titlecase(res["film"]["scene"][i]["scene_contents"]["dialog"][j]["char_name"].strip()))
    '''
    if ("slug" in res["film"]["scene"][i]["scene_start"].keys()):
        scene_locations.append(res["film"]["scene"][i]["scene_start"]["slug"].strip())
    '''
    print res["film"]["scene"][i]["scene_contents"].keys()
    if ("dialog" in res["film"]["scene"][i]["scene_contents"].keys()):
        print res["film"]["scene"][i]["scene_contents"]["dialog"][0]["char_name"]
    if ("description" in res["film"]["scene"][i]["scene_contents"].keys()):
        print res["film"]["scene"][i]["scene_contents"]["description"].keys()
    for j in range(0,len(res["film"]["scene"][i]["scene_contents"])):
        print res["film"]["scene"][i]["scene_contents"][j].getName()
    '''
    #characters_in_scenes.append(characters)
    content = ""
    for scene_content in res["film"]["scene"][i]["scene_contents"]:
	if scene_content.getName() == "description":
	    content = content + " " + process(scene_content[0])
	elif scene_content.getName() == "char_name":
	    content = content + " " + process_dialogue(scene_content)
    f=open(scenes_folder+"/"+str(i)+".txt","w")
    f.write(content)
    f.close()
    #full_content = full_content + " " + content

#f=open("full/full_text.txt","w")
#f.write(full_content)
#f.close()

parsed=list(batch_parse(scenes_folder+"/","stanford-corenlp-full-2014-08-27/"))
#parsed=list(batch_parse("full/","stanford-corenlp-full-2014-08-27/"))

#characters=Set()

def get_character_names(scene_dict,sentence_no,noun,coref_dict,nn,conj_and,characters):
    character_names=[]
    iter_list=[(sentence_no,noun)]
    if (sentence_no,noun) in conj_and:
	iter_list=iter_list+conj_and[(sentence_no,noun)]
    for (iter_sentence_no,iter_noun) in iter_list:
	character_name=""
        if scene_dict['sentences'][iter_sentence_no]['words'][iter_noun][1]['NamedEntityTag'] == "PERSON":
	    if (iter_sentence_no,iter_noun) in nn:
	        character_words_index=nn[(iter_sentence_no,iter_noun)]
	        character_words_index.append((iter_sentence_no,iter_noun))
	        character_words_index.sort()
	        character_words=[scene_dict['sentences'][i]['words'][j][0] for (i,j) in character_words_index]
	        character_name=titlecase(" ".join(character_words))
	    else:
	        character_name=titlecase(scene_dict['sentences'][iter_sentence_no]['words'][iter_noun][0])
        elif (iter_sentence_no,iter_noun) in coref_dict:
	    character_words=[]
	    s_no=coref_dict[(iter_sentence_no,iter_noun)][0]
	    word_start=coref_dict[(iter_sentence_no,iter_noun)][1]
	    word_end=coref_dict[(iter_sentence_no,iter_noun)][2]
	    for i in range(word_start,word_end):
	        if scene_dict['sentences'][s_no]['words'][i][1]['NamedEntityTag'] == "PERSON":
		    character_words.append(scene_dict['sentences'][s_no]['words'][i][0].split("'")[0])
		    character_name=titlecase(" ".join(character_words))
	if len(character_name)>0 and character_name in characters:
	    character_names.append(character_name)
    return character_names

def make_an_edge(adjacency_list,prev_node,curr_node,link_type):
    if link_type=="cross":
	if (prev_node,"cross") not in adjacency_list[curr_node][1]: adjacency_list[curr_node][1].append((prev_node,"cross"))
        if (curr_node,"cross") not in adjacency_list[prev_node][1]: adjacency_list[prev_node][1].append((curr_node,"cross"))
    elif link_type=="causal":
        if adjacency_list[prev_node][0]==0 and adjacency_list[curr_node][0]==1:
	    adjacency_list[prev_node][1].append((curr_node,"a"))
        elif adjacency_list[prev_node][0]==0 and adjacency_list[curr_node][0]==-1:
	    adjacency_list[prev_node][1].append((curr_node,"a"))
        elif adjacency_list[prev_node][0]==-1 and adjacency_list[curr_node][0]==0:
	    adjacency_list[prev_node][1].append((curr_node,"m"))
        elif adjacency_list[prev_node][0]==0 and adjacency_list[curr_node][0]==0:
	    adjacency_list[prev_node][1].append((curr_node,"m"))
        elif adjacency_list[prev_node][0]==1 and adjacency_list[curr_node][0]==0:
	    adjacency_list[prev_node][1].append((curr_node,"m"))

def create_new_plot_units(plot_units,characters,adjacency_list,sentence_no,word_no,affect_state,node_no):
    for character in characters:
        adjacency_list.append([affect_state,[]])
	if character in plot_units:
            plot_units[character].append((sentence_no,word_no,affect_state,node_no))
	    prev_node=plot_units[character][-2][3]
	    curr_node=node_no
	    make_an_edge(adjacency_list,prev_node,curr_node,"causal")
	else:
	    plot_units[character]=[(sentence_no,word_no,affect_state,node_no)]
	node_no=node_no+1
    return node_no

def create_cross_links(plot_units,adjacency_list):
    for first_character in plot_units:
	for second_character in plot_units:
	    if first_character!=second_character:
	        for first_plot_unit in plot_units[first_character]:
	            for second_plot_unit in plot_units[second_character]:
	   		first_sentence_no=first_plot_unit[0]
	   		second_sentence_no=second_plot_unit[0]
	   		first_node=first_plot_unit[3]
	   		second_node=second_plot_unit[3]
	   		if abs(first_sentence_no-second_sentence_no)<=2:
	   		    make_an_edge(adjacency_list,first_node,second_node,"cross")

def dfs1(node,adjacency_list,vis,stack):
    vis[node]=1
    for (child_node,link_type) in adjacency_list[node][1]:
	if vis[child_node]==0:
	    dfs1(child_node,adjacency_list,vis,stack)
    stack.append(node)

def dfs2(node,adjacency_list,vis,scc):
    vis[node]=1
    scc.append(node)
    for (child_node,link_type) in adjacency_list[node][1]:
	if vis[child_node]==0:
	    dfs2(child_node,adjacency_list,vis,scc)

def create_reverse_adjacency_list(adjacency_list):
    reverse_adjacency_list=[(affect_state,[]) for (affect_state,ll) in adjacency_list]
    for node in range(0,len(adjacency_list)):
	for (child_node,link_type) in adjacency_list[node][1]:
	    reverse_adjacency_list[child_node][1].append((node,link_type))
    return reverse_adjacency_list

def create_complete_adjacency_list(adjacency_list):
    complete_adjacency_list=[(affect_state,ll) for (affect_state,ll) in adjacency_list]
    for node in range(0,len(adjacency_list)):
	for (child_node,link_type) in adjacency_list[node][1]:
	    complete_adjacency_list[child_node][1].append((node,link_type))
    return complete_adjacency_list

def strongly_connected_components(scene_no,adjacency_list,plot_units,scene_dict):
    print "Scene #",scene_no
    print "Number of nodes:", len(adjacency_list)
    complete_adjacency_list=create_complete_adjacency_list(adjacency_list)
    vis=[0]*len(complete_adjacency_list)
    scc_count=0
    for node in range(0,len(complete_adjacency_list)):
	scc=[]
	if vis[node]==0:
	    scc_count=scc_count+1
	    dfs2(node,complete_adjacency_list,vis,scc)
	    print "SCC #",scc_count,"Number of nodes:",len(scc)
	    scc.sort()
	    for node in scc:
		found=False
		for character in plot_units.keys():
		    for plot_unit in plot_units[character]:
		        if plot_unit[3]==node:
		    	    sentence_no=plot_unit[0]
		    	    word_no=plot_unit[1]
		    	    affect_state=plot_unit[2]
		            print "(",character,scene_dict['sentences'][sentence_no]['words'][word_no][0]+'-'+str(sentence_no)+'-'+str(word_no),affect_state,node,")",
		    	    found=True
		            break
		    if found: break
	    print
    '''
    reverse_adjacency_list=create_reverse_adjacency_list(adjacency_list)
    stack=[]
    vis=[0]*len(adjacency_list)
    for node in range(0,len(adjacency_list)):
	if vis[node]==0: dfs1(node,adjacency_list,vis,stack)
    vis=[0]*len(adjacency_list)
    scc_count=0
    while (len(stack)>0):
	node=stack.pop()
	if vis[node]==0:
	    scc_count=scc_count+1
	    scc=[]
	    dfs2(node,reverse_adjacency_list,vis,scc)
	    print "SCC #",scc_count,"Number of nodes:",len(scc)
	    for node in scc:
		found=False
		for character in plot_units.keys():
		    for plot_unit in plot_units[character]:
		        if plot_unit[3]==node:
		    	    sentence_no=plot_unit[0]
		    	    word_no=plot_unit[1]
		    	    affect_state=plot_unit[2]
		            print "(",character,scene_dict['sentences'][sentence_no]['words'][word_no][0]+'-'+str(sentence_no)+'-'+str(word_no),affect_state,node,")",
		    	    found=True
		            break
		    if found: break
	    print
	    scc=[]
    '''

def draw_graphs(scene_no,plot_units,adjacency_list):
    dg=digraph()
    for character_name in plot_units:
	for plot_unit in plot_units[character_name]:
	    affect_state=plot_unit[2]
	    node_no=plot_unit[3]
	    dg.add_node(node_no,[("label",affect_state),("character_name",character_name)])
    for node_no in range(len(adjacency_list)):
	for (child_node,link_type) in adjacency_list[node_no][1]:
	    dg.add_edge((node_no,child_node),1,link_type,[])
    graph=gv.readstring(write(dg))
    gv.layout(graph,'dot')
    gv.render(graph,'png',graphs_folder+"/"+str(scene_no)+".png")



pos_verbs=[]
neg_verbs=[]
neutral_verbs=[]
f=open("positive_words","r")
for pos_verb in f:
    pos_verbs.append(int(pos_verb.strip().lower()))
f.close()
f=open("negative_words","r")
for neg_verb in f:
    neg_verbs.append(int(neg_verb.strip().lower()))
f.close()
f=open("neutral_words","r")
for neutral_verb in f:
    neutral_verbs.append(int(neutral_verb.strip().lower()))
f.close()


for scene_dict in parsed:
    coref_dict={}
    characters=Set()
    adjacency_list=[]
    nn={}
    neg=[]
    conj_and={}
    plot_units={}
    scene_no=int(scene_dict['file_name'].split(".")[0])

#coreferences in scenes
    if "coref" in scene_dict:
        for coref in scene_dict["coref"]:
            for each_coref in coref:
	        t=(each_coref[1][1],each_coref[1][3],each_coref[1][4])
	        for word_index in range(each_coref[0][3],each_coref[0][4]):
		    coref_dict[(each_coref[0][1],word_index)]=t;

    if "sentences" in scene_dict:
        for sentence_no in range(0,len(scene_dict['sentences'])):
	    sentence=scene_dict['sentences'][sentence_no]
	    character_name=""
	    word_no=0

#character names in scene
	    for word_no in range(0,len(sentence['words'])):
		word=sentence['words'][word_no]
	        if word[1]['NamedEntityTag'] == "PERSON":
	            if len(character_name)>0:
	                character_name=character_name+" "+word[0].split("'")[0]
	            else:
	                character_name=word[0]
	        else:
	            if len(character_name)>0:
		        if (sentence_no,word_no-1) not in coref_dict:
	                    characters.add(titlecase(character_name.strip()))
			else:
			    character_words=[]
			    s_no=coref_dict[(sentence_no,word_no-1)][0]
			    word_start=coref_dict[(sentence_no,word_no-1)][1]
			    word_end=coref_dict[(sentence_no,word_no-1)][2]
			    for i in range(word_start,word_end):
				if scene_dict['sentences'][s_no]['words'][i][1]['NamedEntityTag'] == "PERSON":
				    character_words.append(scene_dict['sentences'][s_no]['words'][i][0].split("'")[0])
			    if (len(character_words)>0):
				characters.add(titlecase(" ".join(character_words)))
	            character_name=""

	    if len(character_name)>0:
	        if (sentence_no,word_no) not in coref_dict:
	            characters.add(titlecase(character_name.strip()))
		else:
		    character_words=[]
		    s_no=coref_dict[(sentence_no,word_no)][0]
		    word_start=coref_dict[(sentence_no,word_no)][1]
		    word_end=coref_dict[(sentence_no,word_no)][2]
		    for i in range(word_start,word_end):
		        if scene_dict['sentences'][s_no]['words'][i][1]['NamedEntityTag'] == "PERSON":
			    character_words.append(scene_dict['sentences'][s_no]['words'][i][0].split("'")[0])
		    if (len(character_words)>0):
			characters.add(titlecase(" ".join(character_words)))

#List of negated words, dependency nouns and conjunctions
	    for dependency in sentence['indexeddependencies']:
	        if dependency[0] == "nn":
		    head_noun=int(dependency[1].split("-")[-1])-1
		    modifier_noun=int(dependency[2].split("-")[-1])-1
		    if head_noun in nn:
		        nn[sentence_no,head_noun].append((sentence_no,modifier_noun))
	            else:
		        nn[sentence_no,head_noun]=[(sentence_no,modifier_noun)]
	        elif dependency[0] == "neg":
		    head_word=int(dependency[1].split("-")[-1])-1
		    neg.append((sentence_no,head_word))
	        elif dependency[0] == "conj_and":
		    first_word=int(dependency[1].split("-")[-1])-1
		    second_word=int(dependency[2].split("-")[-1])-1
		    if (sentence_no,first_word) in conj_and:
		        conj_and[(sentence_no,first_word)].append((sentence_no,second_word))
	            else:
		        conj_and[(sentence_no,first_word)]=[(sentence_no,second_word)]
	
#Plot units and graph creation
	node_no=0
	for sentence_no in range(0,len(scene_dict['sentences'])):
	    sentence=scene_dict['sentences'][sentence_no]
	    word_tokens=sentence['text']
#print word_tokens
	    for word_no in range(0,len(sentence['words'])):
		word=sentence['words'][word_no]
		if word[1]['PartOfSpeech'][0] == 'V':
		    affect_state="unknown"
		    #if verb is be find the actual verb it represents which might in a preposition dependency in noun form
		    if word[1]['Lemma'] == "be":
		        for dependency in sentence['indexeddependencies']:
		            relation=dependency[0]
		            governor=int(dependency[1].split("-")[-1])-1
		            dependent=int(dependency[2].split("-")[-1])-1
			    if governor == word_no and "prep" in relation:
				verb=sentence['words'][dependent][1]["Lemma"]
				if len(wn.synsets(verb,pos=wn.VERB)) > 0:
				    offset=wn.synsets(verb,pos=wn.VERB)[0].offset()
				    if offset in pos_verbs: affect_state=1
				    elif offset in neg_verbs: affect_state=-1
				    elif offset in neutral_verbs: affect_state=0
		    else:
		        syn=lesk(word_tokens,word[1]["Lemma"],"v")
			if syn:
				offset=syn.offset()
				if offset in pos_verbs: affect_state=1
				elif offset in neg_verbs: affect_state=-1
				elif offset in neutral_verbs: affect_state=0
		    if affect_state=="unknown": continue
		    #if verb is negated
		    if (sentence_no,word_no) in neg:
			affect_state=-affect_state
		    #extract agents and patients for the verb
		    for dependency in sentence['indexeddependencies']:
		        agents=[]
		        patients=[]
		        relation=dependency[0]
		        governor=int(dependency[1].split("-")[-1])-1
		        dependent=int(dependency[2].split("-")[-1])-1
		        if governor == word_no:
		            if relation=="nsubj" or relation=="agent":
		                agents=get_character_names(scene_dict,sentence_no,dependent,coref_dict,nn,conj_and,characters)
		            elif relation=="nsubjpass" or relation=="dobj":
		                patients=get_character_names(scene_dict,sentence_no,dependent,coref_dict,nn,conj_and,characters)
			#rule no.3
		        if len(agents)>0 and len(patients)>0:
		 	    start_node=node_no
		 	    agent_affect_state="not known"
		 	    patient_affect_state="not known"
		 	    if affect_state==0:
		 		agent_affect_state=0
		 		patient_affect_state=0
		 	    elif affect_state==1:
		 		agent_affect_state=1
		 		patient_affect_state=1
		 	    elif affect_state==-1:
		 		agent_affect_state=1
		 		patient_affect_state=-1
		            if agent_affect_state != "not known":
		                node_no=create_new_plot_units(plot_units,agents,adjacency_list,sentence_no,word_no,agent_affect_state,node_no)
		 	    if patient_affect_state != "not known":
		                node_no=create_new_plot_units(plot_units,patients,adjacency_list,sentence_no,word_no,patient_affect_state,node_no)
			    end_node=node_no
			    for i in range(start_node,end_node):
				for j in range(start_node,end_node):
				    if i!=j:
				        make_an_edge(adjacency_list,i,j,"cross")
			#rule no.1
			elif len(agents)>0:
		            node_no=create_new_plot_units(plot_units,agents,adjacency_list,sentence_no,word_no,affect_state,node_no)
			#rule no.2
		        elif len(patients)>0:
		            node_no=create_new_plot_units(plot_units,patients,adjacency_list,sentence_no,word_no,affect_state,node_no)
	create_cross_links(plot_units,adjacency_list)
	plot_units_in_scenes[scene_no]=plot_units
        characters_in_scenes[scene_no]=characters
	adjacency_lists_in_scenes[scene_no]=adjacency_list
        #strongly_connected_components(scene_no,adjacency_list,plot_units,scene_dict)
	#print adjacency_list
	draw_graphs(scene_no,plot_units,adjacency_list)


for i in range(0,len(characters_in_scenes)):
    if (len(characters_in_scenes[i])>0):
        print "Scene #" , (i+1) , ": ", characters_in_scenes[i], scene_locations[i], plot_units_in_scenes[i]


